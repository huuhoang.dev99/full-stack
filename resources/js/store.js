import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        counter: 1000,
        deleteModalObj: {
            showDeleteModal: false,
            deleteUrl: "",
            data: null,
            deleteIndex: -1,
            isDeleted: false
        },
        user: false,
        userPermission: null
    },
    getters: {
        getCounter(state) {
            // if (state.counter > 1000) return 'Too Big'
            // return 'ok that is small'
            return state.counter;
        },
        getDeleteModalObj(state) {
            console.log("get modal delete Obj");
            return state.deleteModalObj;
        },
        getUserPermission(state) {
            return state.userPermission;
        }
    },
    actions: {
        changeCounterAction({ commit }, data) {
            commit("changeTheCounter", data);
        }
    },
    mutations: {
        changeTheCounter(state, data) {
            state.counter += data;
        },
        setDeleteModal: (state, data) => {
            const deleteModalObj = {
                showDeleteModal: false,
                deleteUrl: "",
                data: null,
                deleteIndex: state.deleteModalObj.deleteIndex,
                isDeleted: data
            };
            state.deleteModalObj = deleteModalObj;
            // state.deleteModalObj.isDeleted = data;
        },
        setDeleteModalObj: (state, data) => {
            state.deleteModalObj = data;
        },
        setUpdateUser: (state, data) => {
            state.user = data;
        },
        setUserPermission(state, data) {
            state.userPermission = data;
        }
    }
});