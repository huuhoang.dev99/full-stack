<?php

use App\Http\Middleware\AdminCheck;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::any('{slug}',function(){
//     return view('welcome');
// });


Route::prefix('app')->middleware(AdminCheck::class)->group(function () {

    Route::post('/create_tag', 'AdminController@addTag');
    Route::get('/get_tags', 'AdminController@getTags');
    Route::post('/edit_tag', 'AdminController@editTag');
    Route::post('/delete_tag', 'AdminController@deleteTag');

    Route::post('/upload', 'AdminController@upload');
    Route::post('/delete_image', 'AdminController@deleteImage');

    Route::post('/create_category', 'AdminController@addCategory');
    Route::get('/get_category', 'AdminController@getCategory');
    Route::post('/edit_category', 'AdminController@editCategory');
    Route::post('/delete_category', 'AdminController@deleteCategory');

    Route::post('/create_user', 'AdminController@createUser');
    Route::get('/get_users', 'AdminController@getUsers');
    Route::post('/edit_user', 'AdminController@editUser');

    //login
    Route::post('/admin_login', 'AdminController@login');

    //role route
    Route::post('create_role', 'AdminController@addRole');
    Route::get('get_roles', 'AdminController@getRoles');
    Route::post('/edit_role', 'AdminController@editRole');
    Route::post('/assignRole', 'AdminController@assignRole');
});

Route::post('createBlog', 'AdminController@uploadEditorImage');
Route::get('slug', 'AdminController@slug');

//blog

Route::post('app/create-blog', 'AdminController@createBlog');
Route::get('blogdata', 'AdminController@blogdata');
Route::get('/', 'AdminController@index');
Route::get('/logout', 'AdminController@logout');
Route::any('{slug}', 'AdminController@index');
